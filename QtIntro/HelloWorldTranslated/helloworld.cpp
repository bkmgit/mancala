// Copyright (C) 2016 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR BSD-3-Clause
// https://doc.qt.io/qt-6/qtlinguist-hellotr-example.html
// https://code.qt.io/cgit/qt/qttools.git/tree/examples/linguist/hellotr/main.cpp?h=6.5
#include <QApplication>
#include <QPushButton>
#include <QTranslator>

int main(int argc, char *argv[]) {

	QApplication app(argc, argv);
	QTranslator translator;
	Q_UNUSED(translator.load("hellotr_gr"));
	app.installTranslator(&translator);
	QPushButton hello(QPushButton::tr("Hi!"));
	hello.resize(250, 150);
	hello.show();

	return app.exec();
}
