// Copyright 2023, Benson Muite and contributors
// SPDX-License-Identifier: MIT

#include <QTextStream>
#include <QString>
#include <QTime>
#include <QRandomGenerator>

void printBoard(int board[]) {
	QTextStream out(stdout);

	out << "       1" << Qt::endl;
	out << "  | " ; 
	for (int i=0; i<6; i++) {	
		out << board[5-i] << " | ";
	}
	out << Qt::endl;
	out << board[6] << " | ";
	for (int i=0; i<6; i++) {
		out << "  | ";
	}
	out << board[13] << Qt::endl;
	out << "  | " ;
	for (int i=0; i<6; i++) {
		out << board[7+i] << " | ";
	}
	out << Qt::endl;
	out << "        2" << Qt::endl;
}

int getMove(int board[], int player) {
	QTextStream in(stdin);
	QTextStream out(stdout);

	int startHole;
	bool inputError = true;
	out << "Player " << player << " choose a hole" << Qt::endl;
	if ( player == 1 ) {
		out << "   | 5 | 4 | 3 | 2 | 1 | 0 |" << Qt::endl;
	} else {
		out << "   | 0 | 1 | 2 | 3 | 4 | 5 |" << Qt::endl;
	}

	while (inputError) {
		in >> startHole;
		if ((startHole >= 0 ) & (startHole <= 5) ) {
			if ((player == 1) & (board[startHole] > 0)) {
				inputError=false;
			} else if ((player == 2) & (board[startHole+7] > 0)) {
				inputError=false;
			} else {
				out << "Please choose a hole with seeds"
					<< Qt::endl;
			}
		} else {
			out << "Please enter a value between 0 and 5"
				<< Qt::endl;
		}
	}

	return startHole;
}

int updateBoard(int board[], int player, int startHole) {

        int seeds;
	int fill=0;
	int finalHole;
        if (player == 1) {
		seeds = board[startHole];
		board[startHole] = 0;
	} else {
		seeds = board[startHole+7];
		board[startHole+7] = 0;
	}

	// Put seeds in holes
	for(int i = 0; i<seeds; i++) {
		if (player == 1) {
			fill = i+startHole+1;
			// skip opponents store
			if (fill == 13) fill += 1;
			fill = fill%14;
		} else {
			fill = i + startHole + 7 + 1;
			// skip opponents store
			if (fill == 6) fill += 1;
			fill = fill%14;
		}
		board[fill] += 1;
	}
	finalHole = fill;

	// Check if a capture of seeds has occured
	// and determine who the next player is
	int oppositeHole = 12 - finalHole;
	if ((finalHole != 6) && (player == 1)) {
		player = 2;
		// check capture
		if ((board[finalHole] == 1) & (finalHole <= 6)) {
			board[6] += board[oppositeHole];
			board[6] += board[finalHole];
			board[finalHole] = 0;
			board[oppositeHole] = 0;
		} 
	}else if((finalHole != 13) && (player == 2)) {
		player = 1;
		// check capture
		if ((board[finalHole] == 1) & (finalHole >= 7)) {
			board[13] += board[oppositeHole];
			board[13] += board[finalHole];
			board[finalHole] = 0;
			board[oppositeHole] = 0;
		}
	}
	return player;
}

int getComputerMove(int board[], int player) {

	bool inputError=true;
	int startHole;
        QTime time = QTime::currentTime();
        QRandomGenerator rng((uint) time.msec());

	while (inputError) {
		startHole = rng.generate() % 6;
		if ((player == 1) & (board[startHole] > 0)) {
			inputError = false;
		} else if ((player == 2) & (board[startHole+7] > 0)) {
			inputError = false;
		}
	}

	return startHole;
}

bool checkEndGame(int board[]) {

	QTextStream out(stdout);

	bool endGame = false;
	int seedsOnSide[] = {0,0};
	// Check if a player has only empty holes
	for (int i = 0; i < 6; i++) seedsOnSide[0] += board[i];
	for (int i = 7; i < 13; i++) seedsOnSide[1] += board[i];
	if ((seedsOnSide[0] == 0) | (seedsOnSide[1] == 0)) {
		endGame = true;
		// Check the winner
		// Count total seeds in stores
		board[13] += seedsOnSide[1];
		board[6] += seedsOnSide[0];
		if (board[6] == board[13]) {
			out << "A draw" << Qt::endl;
		} else if ( board[6] > board[13] ) {
			out << "Player 1 wins" << Qt::endl;
		} else {
			out << "Player 2 wins" << Qt::endl;
		}
	}
	return endGame;
}

int chooseStartingPlayer() {
	QTextStream in(stdin);
	QTextStream out(stdout);

	int player = 0;
	bool repeat = true;
	while (repeat) {
		out << "Enter 1 for you to start or 2 for the computer to start"
			<< Qt::endl;
		in >> player;
		if ((player == 1) | (player == 2)) repeat = false;
	}

	return player;
}

int main(void) {

/*
	  Player 1
    | 5 | 4 | 3 | 2 | 1 | 0 |
   6|---|---|---|---|---|---|13
    | 7 | 8 | 9 | 10| 11| 12|
          Player 2
*/
	int board[14] = {4,4,4,4,4,4,0,
		         4,4,4,4,4,4,0};

	int player = 1;
	bool endGame = false;
	int startHole = 0;
	player = chooseStartingPlayer();
	while (!endGame) {
		printBoard(board);
		if (player == 1) {
			startHole = getMove(board,player);
		} else {
			startHole = getComputerMove(board,player);
		}
		player = updateBoard(board,player,startHole);
		endGame = checkEndGame(board);
	}

	return 0;
}
