<!DOCTYPE TS><TS>
<context>
  <name>get1</name>
  <message>
    <source>Player </source>
    <translation>Mchezaji</translation>
  </message>
  <name>get2</name>
  <message>
    <source> choose a hole</source>
    <translation> chagua shimo</translation>
  </message>
  <name>get3</name>
  <message>
    <source>Please choose a hole with seeds</source>
    <translation>Tafadhali chagua shimo ina mbegu</translation>
  </message>
  <name>get4</name>
  <message>
    <source>Please enter a value between 0 and 5</source>
    <translation>Tafadhali ingiza nambari katika 0 na 5</translation>
  </message>
  <name>end1</name>
  <message>
    <source>A draw</source>
    <translation>Mchezo ni sare</translation>
  </message>
  <name>end2</name>
  <message>
    <source>Player 1 wins</source>
    <translation>Mshindi ni mchezaji 1</translation>
  </message>
  <name>end3</name>
  <message>
    <source>Player 2 wins</source>
    <translation>Mshindi ni mchezaji 2</translation>
  </message>
  <name>start1</name>
  <message>
    <source>Enter 1 for you to start or 2 for the computer to start</source>
    <translation>Ingiza 1 kuanza au 2 kichakato ianze</translation>
  </message>
</context>
</TS>
